import com.vendas.model.Cargo;
import com.vendas.model.Funcionario;
import com.vendas.model.Venda;


public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public Main() {
				
		//fonte https://www.alura.com.br/artigos/reduzindo-de-n-ifs-para-nenhum-com-strategy-em-java
		Funcionario atendente = new Funcionario();
		atendente.setNome("Maria da Silva"); 
		atendente.setSalario(1200.00); 
		atendente.setCargo(Cargo.ATENDENTE);

		Venda novaVenda = new Venda(atendente, 200.0);

		System.out.println("valor da comiss�o: " + novaVenda.calculaComissao());
		
		
		
	}

}